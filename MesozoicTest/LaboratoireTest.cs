﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MesozoicSolution;

namespace MesozoicTest
{
    [TestClass]
    public class LaboratoireTest
    {
        [TestMethod]
        public void TestCreateDinosaur()
        {
            
            Dinosaur Louis = Laboratoire.CreateDinosaur<Stegausaurus>("Louis",12);
            Assert.AreEqual("Louis", Louis.getName());
            Assert.AreEqual("Stegausaurus", Louis.getSpecie());
            Assert.AreEqual(12, Louis.getAge());
        }
    }
}
