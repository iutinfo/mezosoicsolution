﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MesozoicSolution
{
    public class Laboratoire
    {
        public static TDinosaur CreateDinosaur<TDinosaur>(string name, int age = 0) where TDinosaur : Dinosaur
        {
            return (TDinosaur)Activator.CreateInstance(typeof(TDinosaur), name, age);
        }
    }
}
