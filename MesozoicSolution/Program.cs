﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MesozoicSolution
{
    class Program
    {
        static void Main(string[] args)
        {
            
            //Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Dinosaur nessie = new Diplodocus("Nessie", 11);

            /*List<Dinosaur> dinosaurs = new List<Dinosaur>();

            dinosaurs.Add(louis);
            dinosaurs.Add(nessie);

            Console.WriteLine(dinosaurs.Count);

            foreach (Dinosaur dino in dinosaurs)
            {
                Console.WriteLine(dino.sayHello());
            }

            dinosaurs.Remove(louis);

            Console.WriteLine(dinosaurs.Count);
            foreach (Dinosaur dino in dinosaurs)
            {
                Console.WriteLine(dino.sayHello());
            }
            *

            Hord cretace = new Hord();
            cretace.addDino(nessie);
            cretace.addDino(louis);

            cretace.sayHello();

            cretace.removeDino(louis);

            cretace.sayHello();

            Dinosaur henry = new Diplodocus("Henry", 11);
            Console.WriteLine(henry.sayHello());
            Dinosaur louis = new Stegausaurus("Louis", 12);
            Console.WriteLine(louis.sayHello());
            Console.WriteLine(henry.sayHello());

            Laboratoire labo = new Laboratoire();
            Dinosaur Jean = labo.CreateDinosaur("Jean", "T-rex", 25);
            Console.WriteLine(Jean.sayHello());
            Dinosaur René = labo.CreateDinosaur("René", "Raptor", 2);
            Dinosaur Flo = labo.CreateDinosaur("Flo", "Dabosaurus", 20);
            Console.WriteLine(René.sayHello());
            Console.WriteLine(Flo.sayHello());*/
            /*
            Dinosaur louis = new Stegausaurus("Louis", 12);
            Dinosaur louis2 = new Stegausaurus("Louis", 12);
            Console.WriteLine(louis);
            Console.WriteLine(louis == louis2);
            louis2 = louis;
            Console.WriteLine(louis == louis2);
            */
            /*Dinosaur louis = new Stegausaurus("Louis", 12);
            Dinosaur louis2 = new Stegausaurus("Louis", 12);
            Dinosaur nessie = new Diplodocus("Nessie", 11);

            Console.WriteLine(louis.GetHashCode()); // 2034845202
            Console.WriteLine(louis2.GetHashCode()); //2034845202
            Console.WriteLine(nessie.GetHashCode()); //-275418933*/

            Dinosaur dinosaur = new Stegausaurus("Louis", 12);
            Stegausaurus dinosaur2 = new Stegausaurus("Louis", 12);
            Dinosaur dinosaur3 = new Diplodocus("Louis", 12);
            Console.WriteLine(dinosaur == dinosaur2); // True
            Console.WriteLine(dinosaur == dinosaur3); // False

            Laboratoire.CreateDinosaur<Diplodocus>("Nessie", 11);
            Laboratoire.CreateDinosaur<Triceratops>("Louis", 12);

            Console.ReadKey();
        }
    }
}
