﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MesozoicSolution
{
    public abstract class Dinosaur
    {
        protected string name;
        protected virtual string specie { get { return "Dinosaur";} }
        protected int age;


        public Dinosaur(string name, string specie, int age)
        {
            this.name = name;
            
            this.age = age;
        }

        public virtual string roar()
        {
            return "Grrr";
        }

        public virtual string sayHello()
        {
            return String.Format("Je suis {0} le {1}, j'ai {2} ans.", this.name, specie, this.age);
        }

        public string getName()
        {
            return this.name;
        }

        public int getAge()
        {
            return this.age;
        }
        public string getSpecie()
        {
            return specie;
        }

        public void setName(string newname)
        {
            this.name = newname;
        }

        public void setAge(int newage)
        {
            this.age = newage;
        }

        public string hug(Dinosaur dinosaur)
        {
            return String.Format("Je suis {0} et je fais un calin à {1}", this.getName(), dinosaur.getName());
        }

        public override int GetHashCode()
        {
            int hash = 13;
            hash ^= this.name.GetHashCode();
            hash ^= this.age.GetHashCode();
            hash ^= this.specie.GetHashCode();
            return hash;
        }

        public static bool operator ==(Dinosaur dino1, Dinosaur dino2)
        {
            return dino1.name == dino2.name && dino1.specie == dino2.specie && dino1.age == dino2.age;
        }
        public static bool operator !=(Dinosaur dino1, Dinosaur dino2)
        {
            return !(dino1 == dino2);
        }
        public override bool Equals(object obj)
        {
            return obj is Dinosaur && this == (Dinosaur)obj;

        }

    }
}

