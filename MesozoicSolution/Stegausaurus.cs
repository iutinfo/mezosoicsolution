﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MesozoicSolution
{
    public class Stegausaurus : Dinosaur
    {
        public Stegausaurus(string name, int age) :
            base(name, "Stegausaurus", age)
        {
        }
        protected override string specie { get { return "Stegausaurus"; } }
        public override string roar()
        {
            return "Niarkniark";
        }
        public override string sayHello()
        {
            return String.Format("Je suis {0}, le Stego, j'ai {1} ans xptdr", this.name, this.age);
        }

      
        public override string ToString()
        {
            
            return String.Format("{0} = name: {1}, age: {2}", base.ToString(), this.name, this.age);
        }

}
}
